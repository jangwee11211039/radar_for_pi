#include"Utils.h"


//清空过程数据数据
void Utils::clear_process_data(void)
{
	first_zone_division_theta_.clear();
	first_zone_division_rho_.clear();
	second_zone_division_rho_.clear();
	second_zone_division_theta_.clear();
	select_points_.clear();
}


//一次区域分割
void Utils::first_zone_division(void)
{
	long last_data=-1;
	long now_data=-1;
	std::cout<<"!!!!!!!!!"<<capture_data_.size()<<std::endl;
	for (vector<long>::size_type i=0;i<capture_data_.size();++i)
	{
		now_data=capture_data_.at(i);
		if ((now_data>inner_rho_)&&(now_data<outer_rho_))
		{
			if ((abs(now_data-last_data))<first_zone_divison_rho_threshold_)
			{
				int temp_1=now_data;
				//这个地方是根据公式 将索引换算成弧度值 当前扫描范围[0 pi] 
				//对应控制实例中的deg2step(index)中index范围[-90 90]
				double temp_2=((double)i/4.0/*-45*/)*PI/180;
				first_zone_division_rho_.push_back(temp_1);
				first_zone_division_theta_.push_back(temp_2);
							}	
			else
			{
				first_zone_division_rho_.push_back(-1);
				first_zone_division_theta_.push_back(1000);
			}
			

		}
		last_data=now_data;
	}
	//为下一级数据处理方便
	first_zone_division_rho_.push_back(-1);
	first_zone_division_theta_.push_back(1000);
}

//二次滤波 点数少的区域滤掉
void Utils::second_zone_filter(void)
{
	int point_count=0;
	vector<double>::const_iterator iter2=first_zone_division_theta_.begin();
	for (vector<int>::const_iterator iter=first_zone_division_rho_.begin();
		iter!=first_zone_division_rho_.end();++iter,++iter2)
	{
		if (*iter>0)
		{
			second_zone_division_rho_.push_back(*iter);
			second_zone_division_theta_.push_back(*iter2);
			point_count++;
		}
		else
		{
			//区域内点数足够多
			if (point_count>=secone_zone_division_threshold)
			{
				second_zone_division_rho_.push_back(-1);
				second_zone_division_theta_.push_back(1000);
			}
			else
			{
				//删除这部分区域插入的数据
				for(int i=0;i<point_count;++i)
				{
					second_zone_division_rho_.pop_back();
					second_zone_division_theta_.pop_back();
				}
			}
			point_count=0;
		}
	}

}



//将二次滤波后的数据抽象成点
void Utils::zone2point()
{
	vector<double> temp_theta;
	vector<int> temp_rho;

	int index_note=0;
	int selected_rho;
	double selected_theta;

	vector<double>::const_iterator iter_theta=second_zone_division_theta_.begin();
	vector<int>::const_iterator iter_rho=second_zone_division_rho_.begin();

	for (;iter_rho!=second_zone_division_rho_.end();++iter_rho,++iter_theta)
	{
		if (*iter_rho>0)
		{
			temp_rho.push_back(*iter_rho);
			temp_theta.push_back(*iter_theta);
			++index_note;
		}
		else
		{
			// 原来是求平均 改为求中间的角度值
			selected_rho=temp_rho.at(index_note/2);
			selected_theta=temp_theta.at(index_note/2);
			select_points_[selected_theta]=selected_rho;
			temp_theta.clear();
			temp_rho.clear();
			index_note=0;
		}
	}
}


//更新全局数据（最近的两个点）
void Utils::update_global_data_near_point()
{
	const int num_select_points=2;
	//临时map 便于排序
	map<int,double> temp_select_points;	

	for (map<double,int>::const_iterator iter=select_points_.begin();
		iter!=select_points_.end();++iter)
	{
		temp_select_points[iter->second]=iter->first;
	}
	
	map<int,double>::const_iterator iter2=temp_select_points.begin();
	//比较危险，没有越界检查，硬编码
	for (int i=0;i<num_select_points;++i,++iter2)
	{
		near_point[i][0]=iter2->first;
		near_point[i][1]=iter2->second;
	}

	std::cout<<"near point---"<<std::endl;
	std::cout<< near_point[0][0] <<"  "<<near_point[0][1] <<"| "<<near_point[1][0]<<" "<<near_point[1][1]<<std::endl;
	std::cout<<"---end"<<std::endl;
}


#ifdef _TEST_

bool Utils::write_data_to_files(const char* file_name_)
{
#if 0
	FILE *fp=NULL;
	if ((fp=fopen(file_name_,"wb+"))==NULL)
	{
		printf_s("[Utils]file which will be writed failed to open");
		return false;
	}

	for (vector<long>::const_iterator iter=capture_data_.begin();
		iter!=capture_data_.end();++iter)
	{
		fprintf_s(fp,"%ld\n\t",*iter);
	}
	fclose(fp);
#endif
	return true;
}

//从文件中读入数据
bool Utils::read_data_from_files(const char* file_name_) 
{
#if 0
	FILE* fp=NULL;
	if((fp=fopen(file_name_,"r"))==NULL)
	{
		printf_s("[Utils]the file which contains radar data failed to opened.will exit");
		return false;
	}
	capture_data_.clear();
	long buffer=0;
	while (!feof(fp))
	{
		fscanf_s(fp,"%ld",&buffer);
		capture_data_.push_back(buffer);
	}

	fclose(fp);
#endif
	return true;
}



//显示原始捕获数据图象
void Utils::create_origin_radar_image(IplImage *radar_image)
{
	cvZero(radar_image);
	//在中心加个圆心
	cvCircle(radar_image,cvPoint(radar_image_width_/2,radar_image_height_/2),3,CV_RGB(0,255,255));
	int x=0;
	int y=0;
	double theta=0.00000;
	double rho=0.00000;
	unsigned char *pPixel=NULL;

	for (vector<long>::size_type i=0;i<capture_data_.size();++i)
	{
		//极坐标转换
		theta=(i/4.0/*-45*/)*PI/180;
		rho=capture_data_.at(i);

		x=(int)(rho*cos(theta)/RF)+radar_image_width_/2;
		y=(int)(-rho*sin(theta)/RF)+radar_image_height_/2;//加负号原因是 绘图的Y的正方向是向下的！

		if (x>=0&&x<radar_image_width_&&y>=0&&y<radar_image_height_)
		{
			pPixel=(unsigned char*)radar_image->imageData+y*radar_image->widthStep+3*x+2;
			*pPixel=255;
		}
	}
}

//显示一次滤波之后的图象
void Utils::create_first_filter_radar_image(IplImage *radar_image_)
{
	cvZero(radar_image_);
	cvCircle(radar_image_,cvPoint(radar_image_width_/2,radar_image_height_/2),3,CV_RGB(0,255,255));


	for (vector<int>::size_type i=0;i<first_zone_division_rho_.size();++i)
	{
		int temp_rho=first_zone_division_rho_.at(i);
		double temp_theta=0;
		int temp_x=0;
		int temp_y=0;
		unsigned char* pPixel=NULL;
		if (temp_rho>0)
		{
			temp_theta=first_zone_division_theta_.at(i);
			temp_x=temp_rho*cos(temp_theta)/RF+radar_image_width_/2;
			temp_y=-temp_rho*sin(temp_theta)/RF+radar_image_height_/2;
		}
		if (temp_x>0&&temp_x<radar_image_width_&&temp_y>0&&temp_y<radar_image_height_)
		{
			pPixel=(unsigned char*)radar_image_->imageData+temp_y*radar_image_->widthStep+3*temp_x+2;
			*pPixel=255;
		}
	}
}



//显示二次滤波之后的图象
void Utils::create_second_filter_radar_image(IplImage *radar_image_)
{
	cvZero(radar_image_);
	cvCircle(radar_image_,cvPoint(radar_image_width_/2,radar_image_height_/2),3,CV_RGB(0,255,255));


	for (vector<int>::size_type i=0;i<second_zone_division_rho_.size();++i)
	{
		int temp_rho=second_zone_division_rho_.at(i);
		double temp_theta=0;
		int temp_x=0;
		int temp_y=0;
		unsigned char* pPixel=NULL;
		if (temp_rho>0)
		{
			temp_theta=first_zone_division_theta_.at(i);
			temp_x=temp_rho*cos(temp_theta)/RF+radar_image_width_/2;
			temp_y=-temp_rho*sin(temp_theta)/RF+radar_image_height_/2;
		}
		if (temp_x>0&&temp_x<radar_image_width_&&temp_y>0&&temp_y<radar_image_height_)
		{
			pPixel=(unsigned char*)radar_image_->imageData+temp_y*radar_image_->widthStep+3*temp_x+2;
			*pPixel=255;
		}
	}
}

//显示拟合特征点位置
void Utils::create_fitting_point_image(IplImage *radar_image_)
{
	cvZero(radar_image_);
	cvCircle(radar_image_,cvPoint(radar_image_width_/2,radar_image_height_/2),3,CV_RGB(0,255,255));
	for (map<double,int>::const_iterator iter=select_points_.begin();iter!=select_points_.end();++iter)
	{
		double temp_theta=iter->first;
		int temp_rho=iter->second;
		int temp_x=temp_rho*cos(temp_theta)/RF;
		int temp_y=temp_rho*sin(temp_theta)/RF;
		cvCircle(radar_image_,cvPoint(temp_x+radar_image_width_/2,-temp_y+radar_image_height_/2),3,CV_RGB(255,255,0));
	}

}

//显示最近的两个特征点
void Utils::create_nearest_point_image(IplImage *radar_image_)
{
	cvZero(radar_image_);
	cvCircle(radar_image_,cvPoint(radar_image_width_/2,radar_image_height_/2),3,CV_RGB(0,255,255));
	for (int i=0;i<2;++i)
	{
		int temp_x=near_point[i][0]*cos(near_point[i][1])/RF;
		int temp_y=near_point[i][0]*sin(near_point[i][1])/RF;
		cvCircle(radar_image_,cvPoint(temp_x+radar_image_width_/2,-temp_y+radar_image_height_/2),3,CV_RGB(0,255,255));
	}
}

#endif







void Utils::display_vector_int_data(const vector<int>& vec)const
{
	for (vector<int>::const_iterator iter=vec.begin();iter!=vec.end();++iter)
	{
		std::cout<<*iter<<" : ";
	}
	std::cout<<std::endl;
}

void Utils::display_vector_double_data(const vector<double>& vec)const
{
	for (vector<double>::const_iterator iter=vec.begin();iter!=vec.end();++iter)
	{
		std::cout<<*iter<<" : ";
	}
	std::cout<<std::endl;
}

void Utils::display_all()const
{
	std::cout<<"first theta::"<<std::endl;
	display_vector_double_data(first_zone_division_theta_);
	std::cout<<"first rho::"<<std::endl;
	display_vector_int_data(first_zone_division_rho_);
	std::cout<<"second theta::"<<std::endl;
	display_vector_double_data(second_zone_division_theta_);
	std::cout<<"second rho::"<<std::endl;
	display_vector_int_data(second_zone_division_rho_);
	std::cout<<"zone2point"<<std::endl;
	std::cout<<"size of map: "<<select_points_.size()<<std::endl;
	for (map<double,int>::const_iterator point_iter=select_points_.begin();
		point_iter!=select_points_.end();++point_iter)
	{
		std::cout<<"X :"<<point_iter->first<<" Y:"<<point_iter->second<<std::endl;
	}
}

void Utils::display_2points()const
{
	for (map<double,int>::const_iterator point_iter=select_points_.begin();
		point_iter!=select_points_.end();++point_iter)
	{
		std::cout<<"X:"<<point_iter->first<<"  Y:"<<point_iter->second<<std::endl;
	}
}


/*

//极坐标到直角坐标的转换
//Parameter: (theta,rho)--->(x,y)
void Utils::polar2xycodr(vector<double>& theta,vector<int>& rho,vector<double>& x,vector<double>& y)
{
	x.clear();
	y.clear();
	vector<double>::iterator iter_theta=theta.begin();
	vector<int>::iterator iter_rho=rho.begin();
	for (;iter_theta!=theta.end();++iter_theta,++iter_rho)
	{
		x.push_back((*iter_rho)*cos(*iter_theta));
		y.push_back((*iter_rho)*sin(*iter_theta));
	}
}



//将数据转成直角坐标系下
void Utils::conver2xycodr(void)
{
	double theta=0.00000;
	double rho=0.00000;
	for (vector<long>::size_type i=0;i<capture_data_.size();++i)
	{
		theta=(i/4.0-45)*PI/180;
		rho=static_cast<double>(capture_data_.at(i));

		data_x_[i]=rho*cos(theta);
		data_y_[i]=rho*sin(theta);
	}

}





//这部分没有实现。原因是怀疑中值滤波会破坏随机性。降低样本的多样性。
void Utils::median_filter()
{

}
//寻找角点
int Utils::ploygon_fitting(double* X_,double* Y_,int size_,double Eps_)
{
	double dis=sqrt((double)((X_[size_-1]-X_[0])*(X_[size_-1]-X_[0])+
		(Y_[size_-1]-Y_[0])*(Y_[size_-1]-Y_[0])));
	double cos_theta=(X_[size_-1]-X_[0])/dis;
	double sin_theta=(Y_[size_-1]-Y_[0])/dis;
	double max_dis=0.0;
	int max_piont_index=0;

	for (int i=1;i<size_-1;++i)
	{
		//坐标旋转，求其他点与拟合直线的距离
		double conv_x=abs((Y_[i]-Y_[1])*cos_theta+(X_[i]-X_[1])*sin_theta);
		if (conv_x>max_dis)
		{
			max_dis=conv_x;
			max_piont_index=i;
		}
	}
	if (max_dis>Eps_)
	{
		return max_piont_index;
	}
	return 0;
}


//通过两圆交点获得位置
//Parameter: 半径-坐标 r1-(x1,y1) r2-(x2,y2) 
//return: (cpx1,cpy1) (cpx2,cpy2) 两个交点 需要剔除
void Utils::search_cross_point(int x1,int y1,int r1,int x2,int y2,int r2,int& cpx1,int& cpy1,int& cpx2,int &cpy2)
{
	//相交直线系数 Y=tempA*X+tempB;
	double tempA=-(double)(x1-x2)/(y1-y2);
	double tempB=0.5*(double)((x1*x1-x2*x2)+(y1*y1-y2*y2)-(r1*r1-r2*r2))/(y1-y2);

	//一元二次方程系数 a*X^2+b*x+c=0
	double tempa=1+tempA*tempA;
	double tempb=2*tempA*tempB-2*x1-2*tempA*y1;
	double tempc=x1*x1+y1*y1+tempB*tempB-2*tempB*y1-r1*r1;

	//一元二次方程组的根
	double temp1=sqrt(tempb*tempb-4*tempa*tempc);              
	cpx1=(-tempb+temp1)/(2*tempa);
	cpx2=(-tempb-temp1)/(2*tempa);
	cpy1=tempA*cpx1+tempB;
	cpy2=tempA*cpx2+tempB;
}

//求旋转角
// Parameter: (X_w0,Y_w0)--雷达的世界位置 (X_w1,Y_w1)--参考点的世界位置 (X_l0,Y_l0)--参考点的在雷达坐标系中的位置
// return: 雷达相对于世界坐标系的旋转角(-pi,pi)
double Utils::search_rotation_angle(int X_w0,int Y_w0,int X_w1,int Y_w1,int X_l0,int Y_l0)
{
	double theta_w=atan2((double)(Y_w1-Y_w0),(double)(X_w1-X_w0));
	double theta_l=atan2((double)(Y_l0),(double)(X_l0));
	return theta_w-theta_l;
}
//数据如何进行剔除？ 暂时没有进行，先把SVM分类器写出来在
//有两个想法 1 可行域 2 根据惯性导航的结果分析
*/



