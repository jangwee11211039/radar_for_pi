#ifndef ZONE_H_
#define ZONE_H_

#include<vector>
using namespace std;


//区域最多的特征点数
//enum{ZoneMaxPointNum=6};
typedef struct
{
	int x;
	int y;
}zone_point;

class Zone{
public:
	//声明友元类，便于访问
	friend class Location;
	Zone(void){}
	virtual ~Zone(void){}

	//初始化区域边界
	void zone_intializ(int zone_x_start,int zone_x_end,int zone_y_start,int zone_y_end);
	//载入已知特征点（顺时针排序）
	void load_world_feature_point(zone_point* point_);
	//载入约束条件(圆形约束)
	//void load_restrict_conditions(zone_point restrict_center_of_circle,int restrict_radius_of_circle);
	//判断是否在此区域
	bool is_in_this_zone(int x_location,int y_location);

private:
	//区域ID：[0 1 2 3 4 5]
	int id_child_zone;
	//区域特征点[为便于遍历，强约束为两个]
	static const unsigned char point_num_=2;
	//区域禁区约束[暂未使用]
	//zone_point restrict_center_of_circle_;
	//int restrict_radius_of_circle_;

	//定位区域范围[根据已知地图信息读入]
	int zone_x_start_;
	int zone_x_end_;
	int zone_y_start_;
	int zone_y_end_;
	//区域的特征点集合点集合[统一顺时针]
	zone_point point_feature_[2];
};

#endif
