/************************************************************************/
/* 地图类(实现时最好在heap上分配内存)                                                                     */

/************************************************************************/

#ifndef LOCATION_H_
#define LOCATION_H_

#include"Zone.h"
#include"Radar.h"

#include<iostream>
#include<cstdio>
#include<stdlib.h>


//红蓝场标识
enum ID_TEAM{
	BLUE_TEAM=0,
	RED_TEAM=1
};

//子区域
 enum ID_ChildZone{
	SEESAW_CHILD_ZONE_1=0,
	SEESAW_CHILD_ZONE_2,
	PLOE_WALK_CHILD_ZONE_1,
	PLOE_WALK_CHILD_ZONE_2,
	SWING_CHILD_ZONE,
	JUNGLE_GYM_CHILD_ZONE
};

class Location
{

public:
	Location(int w_location_x=0,int w_location_y=0,double w_location_theta=0.00):
			w_location_x_(w_location_x),w_location_y_(w_location_y),w_location_theta_(w_location_theta)
			{pchilde_zone=new Zone[6];}
	//virtual ~Location(){free(pchilde_zone);}
	virtual ~Location(){delete [] pchilde_zone;}	

	//世界地图初始化(装入子区域)
	void world_map_inital_(int id_team);
	//确定所在子区域
	int set_id_child_zone();
	//确定搜索区域
	void load_search_zone_boundary(int x_location_from_epigynous_machine,int y_location_from_epigynous_machine);
	//通过遍历确定位置
	int search_for_accurary_location();

	//从上位机获得位置数据
	void get_data_from_epigynous_machine();

	//返回精确位置给上位机[这一部分留主程序中处理]
	//void send_accurary_location_to_epigynous_machine();


private:
	//世界位置参数
	int w_location_x_;
	int w_location_y_;
	double w_location_theta_;
	//位于子区域的ID [0 1 2 3 4 5]
	int id_child_zone_;
	//世界位置精度[单位 mm]
	static const int w_location_accuracy_=1;
	// 子区域数组指针
	Zone* pchilde_zone;

	//遍历搜寻范围[根据上位机发给的模糊地址]
	int search_x_start_;
	int search_x_end_;
	int search_y_start_;
	int search_y_end_;
	//上位机发送地址误差范围[单位：mm]
	static const int error_recv_location_=100;

	//读取配置文件
	bool read_map_configure(const char* file_name);

};
#endif
