//工具类  进行数据处理 算法主体
#ifndef UTILS_H_
#define UTILS_H_

#define PI 3.141592653
#define RF 8.00  //图像缩放系数

#include "Radar.h"

#ifdef _TEST_
#include "cv.h"
#include "highgui.h"
#endif

#include <vector>
#include <map>

using namespace std;

//采集的CaptureTimes组数据
//extern vector<vector<long>> capture_data_vector;
//Capture组数据均值
//extern vector<long> capture_data_;

class Utils
{
public:
	Utils(){}
	virtual ~Utils(){}
	
	//初始化
	//void intial_memory_zone(void);
	//清理中间数据
	void clear_process_data(void);
	//一次分割滤波
	void first_zone_division(void);
	//二次滤波
	void second_zone_filter(void);
	//区域拟合为一点
	void zone2point();
	//更新全局变量（最近的两个点）
	void update_global_data_near_point(void);




#ifdef _TEST_

	//此处用模板类改写
	void display_vector_double_data(const vector<double>&)const;
	void display_vector_int_data(const vector<int>&)const;
	//将数据写入文件
	bool write_data_to_files(const char* file_name);
	//数据从文件读出
	bool read_data_from_files(const char* file_name);


	//二维绘图
	void create_origin_radar_image(IplImage *radar_image_);
	void create_first_filter_radar_image(IplImage *radar_image_);
	void create_second_filter_radar_image(IplImage *radar_image_);
	void create_fitting_point_image(IplImage *radar_image_);
	void create_nearest_point_image(IplImage *radar_image_);

	//打印数据
	void display_all(void)const;
	void display_2points(void)const;

#endif

	//数据二维转化到直角坐标
	void conver2xycodr(void);
	//数据二维转化到极坐标
	//void conver2polarcodr(void);

	//极坐标转换直角坐标(rho,theta,x,y)
	void polar2xycodr(vector<double>&,vector<int>&,vector<double>&,vector<double>&);

	//解二元二次方程组 求交点
	//void search_cross_point(int x1,int x2,int x3,int x4,int r1,int r2,int& cpx1,int &cpy1,int& cpx2,int& cpy2);
	//求旋转角度（全局,以逆时针为正方向）
	//double search_rotation_angle(int X_w0,int Y_w0,int X_w1,int Y_w1,int X_l0,int X_l1);

	//中值滤波
	//void median_filter();
	//多边形拟合 寻找角度点
	//int ploygon_fitting(double* X_,double* Y_,int size_,double Eps_);
private:

	//雷达的感兴趣范围
	static const int inner_rho_=400;
	static const int outer_rho_=1800;

	//数据分解到x y方向
	//double data_x_[1080];
	//double data_y_[1080];

	//一次分割滤波之后的数据
	vector<int> first_zone_division_rho_;
	vector<double> first_zone_division_theta_;
	//一次分割滤波阈值(距离mm)
	static const int first_zone_divison_rho_threshold_=30;

	//二次分割滤波之后的数据
	vector<int> second_zone_division_rho_;
	vector<double> second_zone_division_theta_;
	//二次分割滤波阈值(区域点数)
	static const int secone_zone_division_threshold=2;

	//拟合之后的点集合<theta，rho>
	map<double,int> select_points_;

};

#endif
