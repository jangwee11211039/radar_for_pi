Project: Lidar for Robocon
Version: 1.0
Date of First Created: 2014/2/26
Author: Wee Jang
Email: jangwee11211039@gmail.com

以上均是扯淡。。。。。

下面说一下注意事项：
1 雷达开启 命令行（Linux）:  ./radar [flag::ID_TEAM]
2 上位机发送的数据格式: [0xAA][global_fuzzy_x_coordinate][global_fuzzy_y_coordinate][0x66]
3 下位机发送的数据格式：[0x66][global_accurate_x_coordinate][global_accurate_y_coordinate][0xAA]
			[0x66][0xFF][0XFF][0XAA]
4 地图数据 按照格式填写。UTF-8格式保存，不建议用win下的文本编辑器
5 考虑到树莓派性能以及任务的单一性，没考虑使用POSIX，避免任务切换带来的时延，有兴趣的可以尝试。
