#include"Radar.h"

//串口通信接受缓冲区大小
#define BUFFERLEN 32
#define PROLEN 18 /*1+8*2+1*/

// 一次定位捕获数据次数
enum{CaptureTimes=10};

//调试绘图尺寸
const int radar_image_width_=800;
const int radar_image_height_=800;

//定义上位机位置坐标的全局变量
int x_location_from_epigynous_machine_=0;
int y_location_from_epigynous_machine_=0;
//double theta_from_epigynous_machine_=0.00000;

//定义下位机位置坐标的全局变量
int x_location_according_lisar_radar_=0;
int y_location_according_lisar_radar_=0;
//double theta_according_lisar_radar_=0.00000;

//定义全局变量(两个最近点) [rho,theta] theta降序 顺时针
double near_point[2][2]={{0.1,0.1},{0.1,0.1}};

//时间戳
long timestap=0;
//采集的CaptureTimes组数据
vector< vector<long> > capture_data_vector;
//Capture组数据均值
vector<long> capture_data_;


int main(int argc,char* argv[])
{
	//连接信息
	Connection_information information(argc,argv);
	//控制类实例(V2.0)
	Urg_driver urg;
	//工具类实例
	Utils utils;
	//位置搜寻实例
	Location location;


#ifdef _COM_	
	//串口通信部分
	int fd;
	char recv_buffer[BUFFERLEN];/*0x66 x1 y1 校验*/
	char send_buffer[BUFFERLEN];/*0x66 x1 y1 校验*/
	send_buffer[0]=0x66;

	//串口打开
	if((fd==serialOpen("/dev/ttyAMA0",9600))<0)
	{
		fprintf(stderr,"Unable to open serial device :%s\n",strerror(errno));
		return 1;
	}
	
#endif

	//雷达接口连接
	if(!urg.open(information.device_or_ip_name(),
			      information.baudrate_or_port_number(),
				  information.connection_type())){
		std::cout<<"Urg_driver::open(): "
				 << information.device_or_ip_name()<<":"<<urg.what()<<std::endl;
		return 1;
	}

	//设备参数获取
	cout << "Sensor product type: " << urg.product_type() << endl;
	cout << "Sensor firmware version: " << urg.firmware_version() << endl;
	cout << "Sensor serial ID: " << urg.serial_id() << endl;
	cout << "Sensor status: " << urg.status() << endl;
	cout << "Sensor state: " << urg.state() << endl;

	cout << "step: ["
		<< urg.min_step() << ", "
		<< urg.max_step() << "]" << endl;
	cout << "distance: ["
		<< urg.min_distance()
		<< ", " << urg.max_distance() << endl;

	cout << "scan interval: " << urg.scan_usec() << " [usec]" << endl;
	cout << "sensor data size: " << urg.max_data_size() << endl;
	//时间戳初始化
	timestap=0;
	//全局数据缓存区（CaptureDataBuffer）初始化
	for (int i=0;i<CaptureTimes;++i)
	{
		vector<long> data_capture_index(1080);
		capture_data_vector.push_back(data_capture_index);
	}
	//比赛场地数据载入
	//调试模拟
	//location.world_map_inital_(atoi(argv[3]));
	location.world_map_inital_(BLUE_TEAM);

//扫描范围设置
#if 1 
	urg.set_scanning_parameter(urg.deg2step(-90),urg.deg2step(90),0);
#endif

	while (true)
	{
		//清理原始数据
		for (int i=0;i<CaptureTimes;++i)
		{
			capture_data_vector.at(i).clear();
		}
		capture_data_.clear();
		//清空过程数据
		utils.clear_process_data();


#ifdef _COM_			
		while(1)
		{
			if(serialDataAvail(fd)>=PROLEN)
			{
				if(read(fd,recv_buffer,PROLEN)==PROLEN)
				{	
						
					if(recv_buffer[0]==0X66)
					{	
						//先校验
						char test=0;
						for(int i=0;i<PROLEN-1;++i)
						{
							test^=recv_buffer[i];
						}
						if(test==recv_buffer[PROLEN-1])
						{
							char* precv=(char*)recv_buffer+1;
							sscanf(precv,"%8X%8X",&x_location_from_epigynous_machine_,&y_location_from_epigynous_machine_);
							//清空协议标志
							recv_buffer[0]=0;
							break;
						}	
					}
				}
			}
			else
			{
				delay(10);
			}	
		}
#endif


		x_location_from_epigynous_machine_=10;
		y_location_from_epigynous_machine_=6800;
		//向上位机请求模糊数据
		location.get_data_from_epigynous_machine();
	
		//确定自身所在子区域
		if(location.set_id_child_zone()==-1)
		{       
			//若不在搜寻范围内
#ifdef _TEST_
			std::cout<<"No In Zone"<<std::endl;
#endif
			continue;
		}


		//开始扫描
		urg.start_measurement(Urg_driver::Distance,CaptureTimes,0);
		//获取数据
		for (int i=0;i<CaptureTimes;++i)
		{
			if (!urg.get_distance(capture_data_vector.at(i),&timestap))
			{
				std::cout<<"Urg_driver::get_distance():"<<urg.what()<<std::endl;
				return 1;
			}
		}
#ifdef _TEST_
		
		//查看捕获数据数量
		std::cout<<"捕获数据量   ";
		for (int i=0;i<CaptureTimes;++i)
		{
			std::cout<<capture_data_vector.at(i).size()<<"   ";
		}
		std::cout<<"finish "<<std::endl;
		
#endif		
		//均值计算（去高斯）
		vector<long>::const_iterator iter[CaptureTimes];
		for (int i=0;i<CaptureTimes;++i)
		{
			iter[i]=capture_data_vector.at(i).begin();
		}
		long temp_capture_data=0;
		for (int i=0;iter[0]!=capture_data_vector.at(0).end();++i)
		{
			for (int j=0;j<CaptureTimes;++j)
			{
				temp_capture_data+=(*(iter[j]));
			}
			capture_data_.push_back(temp_capture_data/CaptureTimes);
			temp_capture_data=0;
			for (int j=0;j<CaptureTimes;++j)
			{
				++iter[j];
			}
		}

		//数据一次滤波
		utils.first_zone_division();
		//数据二次滤波
		utils.second_zone_filter();
		
		
		//区域特征点拟合
		utils.zone2point();

#ifdef _TEST_
		printf("\n Main : display selected points: ");
		//utils.display_2points();
		
		printf("\n Main : display all points");
		//utils.display_all();
#endif
	
		//更新最近两点数据（全局变量）
		utils.update_global_data_near_point();
		
		
#ifdef _TEST_
	      //utils.display_all();
		IplImage* radar_image=cvCreateImage(cvSize(radar_image_width_,radar_image_height_),IPL_DEPTH_8U,3);
	//	utils.create_origin_radar_image(radar_image);
	//	utils.create_first_filter_radar_image(radar_image);
	//	utils.create_second_filter_radar_image(radar_image);
	//	utils.create_fitting_point_image(radar_image);
		utils.create_nearest_point_image(radar_image);
		cvNamedWindow("Radar",1);
		cvShowImage("Radar",radar_image);
		int a;
		cvWaitKey(3000);
		cin>>a;
		cvReleaseImage(&radar_image);
		cvDestroyWindow("Radar");	
#endif

		//确定搜寻边界
		location.load_search_zone_boundary(x_location_from_epigynous_machine_,
										  y_location_from_epigynous_machine_);
		//遍历寻找精确位置
		if(location.search_for_accurary_location()==-1)
		{
			//寻点失败，直接不返回，说明不在子区域范围
#ifdef _TEST_
			std::cout<<"not find accurary location"<<std::endl;
#endif
			continue;
			
		}
		else
		{
#ifdef _COM_
			char test=0;
			char* psend=(char*)send_buffer+1;
			sprintf(psend,"%8X",x_location_according_lisar_radar_);
			psend+=8;
			sprintf(psend,"%8X",y_location_according_lisar_radar_);
			for(int i=0;i<PROLEN-1;++i)
			{
				test^=send_buffer[i];
			}
			send_buffer[PROLEN-1]=test;
#endif
			std::cout<<"x:"<<x_location_according_lisar_radar_<<" y:"<<y_location_according_lisar_radar_<<std::endl;
		}

#ifdef _COM_
		while(1)
		{
			if(write(fd,send_buffer,PROLEN)==PROLEN)
			{
				break;
			}
		}

#endif	
		//回复上位机精确数据
		//location.send_accurary_location_to_epigynous_machine();
			
		
		//便于调试暂停
		//int a;
		//cin>>a;

	}

	return 0;
}

