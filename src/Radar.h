#ifndef _RADAR_H_
#define _RADAR_H_

#include"Urg_driver.h"
#include"Connection_information.h"
#include"math_utilities.h"
#include"detect_os.h"
#include"Utils.h"
#include"Location.h"

#include<iostream>
#include<vector>
#include<map>
#include<cmath>
#include<unistd.h>

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<errno.h>

#include<wiringPi.h>
#include<wiringSerial.h>

using namespace std;
using namespace qrk;


#ifdef _TEST_
#include<highgui.h>
#include<cv.h>
#endif

//全局变量表


//调试绘图尺寸
extern const int radar_image_width_;
extern const int radar_image_height_;

//定义上位机位置坐标的全局变量
extern int x_location_from_epigynous_machine_;
extern int y_location_from_epigynous_machine_;
//extern double theta_from_epigynous_machine_;
//定义下位机位置坐标的全局变量
extern int x_location_according_lisar_radar_;
extern int y_location_according_lisar_radar_;
//extern double theta_according_lisar_radar_;

//距离雷达最近的两个点的位置（x,y,theta)
extern double near_point[2][2];



//时间戳
extern long timestap;
//采集的CaptureTimes组数据
extern vector< vector<long> > capture_data_vector;
//Capture组数据均值
extern vector<long> capture_data_;

#endif
