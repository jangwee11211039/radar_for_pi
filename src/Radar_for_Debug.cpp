#include"Urg_dirver.h"
#include"Connection_information.h"
#include"math_utilites.h"
#include"detect_os.h"
#include"Utils.h"
#include"Location.h"

#include<iostream>
#include<vector>
#include<map>
#include<cmath>

#include<cv.h>
#include<highgui.h>

using namespace std;
using namespace qrk;

//一次定位捕获数据次数
enum{CaptureTimes=15};

//调试绘图尺寸
const int radar_image_width_=500;
const int radar_image_height_=500;

//定义上位机位置坐标的全局变量
int x_location_from_epigynous_machine=0;
int y_location_from_epigynous_machine=0;

//定义下位机位置坐标的全局变量
int x_location_according_lisar_radar=0;
int y_location_according_lisar_radar=0;

//定义全局变量（最近大两个点） [rho,theta] theta 降序 顺时针
double near_point[2][2]={{0.1,0.1},{0.1,0.1}};

//时间戳
long timestap=0;

//采集的CaptureTimes组数据
vector< vector<long> > capture_data_vector;

//
