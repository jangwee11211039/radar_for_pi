#include"Zone.h"

void Zone::zone_intializ(int zone_x_start,int zone_x_end,int zone_y_start,int zone_y_end)
{
	zone_x_start_=zone_x_start;
	zone_x_end_=zone_x_end;
	zone_y_start_=zone_y_start;
	zone_y_end_=zone_y_end;
}

void Zone::load_world_feature_point(zone_point* point)
{
	for (int i=0;i<point_num_;++i)
	{
		point_feature_[i].x=(point+i)->x;
		point_feature_[i].y=(point+i)->y;
	}
}

/*
void Zone::load_restrict_conditions(zone_point restrict_center_of_circle,int restrict_radius_of_circle)
{
	restrict_center_of_circle_=restrict_center_of_circle;
	restrict_radius_of_circle_=restrict_radius_of_circle;
}
*/

bool Zone::is_in_this_zone(int x_location,int y_location)
{
	if ((x_location>zone_x_start_)&&(x_location<zone_x_end_)&&(y_location>zone_y_start_)&&(y_location<zone_y_end_))
	{
		return true;
	}
	return false;
}
